import { Button } from 'antd';
import React from 'react';
import logo from './logo.svg';
import styled from '@emotion/styled';
import './App.css';

const StyledDiv = styled.div`
  color: pink;
`;

const App: React.FC = () => {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <StyledDiv>This should be pink</StyledDiv>
        <Button type="primary">Button</Button>
      </header>
    </div>
  );
}

export default App;
